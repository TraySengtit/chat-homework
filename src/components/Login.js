import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Button } from 'react-native';
import auth from '@react-native-firebase/auth';
import { Center, Input, Text } from 'native-base';

function Login({navigation}) {

  // Set an initializing state whilst Firebase connects
  const [initializing, setInitializing] = useState(true);

  const [user, setUser] = useState();

  // user
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  // Handle user state changes
  function onAuthStateChanged(user) {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  if (initializing) return null;

  const logIn = () => {
    auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        console.log('Login successfully !');
        navigation.navigate('chat_room')
      })
      .catch(error => {
        if (error.code === 'auth/user-not-found' || error.code === 'auth/wrong-password') {
          alert('That email address or password is invalid!');
        }
        console.error(error);
      });
  }

  return (
    <View style={styles.container}>
      <Center>
      <Text fontWeight='bold' fontSize='4xl'>Welcome To </Text>
      <Text fontWeight='bold' fontSize='4xl'>Chat Room</Text>
      </Center>
      <Input
        maxW={300}
        style={styles.inputStyle}
        placeholder="Enter Email"
        value={email}
        onChangeText={setEmail}
        autoFocus
      />
      <Input
        maxW={300}
        style={styles.inputStyle}
        placeholder="Enter Passwords"
        value={password}
        onChangeText={setPassword}
        type="password"
      />
      <View style={styles.btnStyle}>
        
        <Button title='Login' onPress={logIn} />
      </View>
        <Button title='Sign Up' onPress={()=>navigation.navigate('singup')} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 50,
    paddingVertical: '80%',
    justifyContent: 'center',
  },
  inputStyle: {
    padding: 2,
    
    marginBottom: 10
  },
  btnStyle: {
    marginBottom: 7,
  }
})

export default Login;