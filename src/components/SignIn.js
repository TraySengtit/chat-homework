import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Button } from 'react-native';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import { Center, Input, Text } from 'native-base';


function SignIn({navigation}) {

  // Set an initializing state whilst Firebase connects
  const [initializing, setInitializing] = useState(true);

  const [user, setUser] = useState();

  // user
  const [email, setEmail] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  // Handle user state changes
  function onAuthStateChanged(user) {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  if (initializing) return null;

  const createUser = () => {
    auth()
      .createUserWithEmailAndPassword(email, password)
      .then((res) => { 
        // console.log("res",res.user.uid);
        let uid = res.user.uid
        firestore()
        .collection('Users')
        .doc(uid)
        .set({
            uid,
            email,
            username,
        })
        alert('User account created !')
        console.log("User created successfully !");
        navigation.navigate('login')
    })
      .catch(error => {
        if (error.code === 'auth/email-already-in-use') {
          alert('That email address is already in use!');
        }
        if (error.code === 'auth/invalid-email') {
          alert('That email address is invalid!');
        }
        if (error.code === 'auth/weak-password') {
          alert('Weak password !');
        }
        console.error(error);
      });
  }

  return (
    <View style={styles.container}>
      <Center>
      <Text fontWeight='bold' fontSize='4xl'>Create Account</Text>
      </Center>
      <Input
        style={styles.inputStyle}
        placeholder="Input Email"
        value={email}
        onChangeText={setEmail}
        autoFocus
      />
      <Input
        style={styles.inputStyle}
        placeholder="Input Username"
        value={username}
        onChangeText={setUsername}
      />
      <Input
        style={styles.inputStyle}
        placeholder="Input Passwords"
        value={password}
        onChangeText={setPassword}
        type="password"
      />
      <Button title='Sign Up' onPress={createUser} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 30,
    paddingVertical: '80%',
    justifyContent: 'center',
  },
  inputStyle: {
    padding: 2,
    marginTop: 5,
    marginBottom: 10
  },
  btnStyle: {
    marginBottom: 7,
  }
})

export default SignIn;