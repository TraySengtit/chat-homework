import { Box, Center, Input, Stack, Text } from 'native-base'
import React from 'react'
import { useState } from 'react'
import firestore from '@react-native-firebase/firestore';
import { Button } from 'react-native';


export const createChatRoom = (roomName, cb) => {
    firestore()
        .collection('MESSAGE_THREADS')
        .add({
            name: roomName,
            latestMessage: {
                text: `${roomName} created. Welcome!`,
                createdAt: new Date().getTime()
            }
        })
        .then(docRef => {
            docRef.collection('MESSAGES').add({
                text: `${roomName} created. Welcome!`,
                createdAt: new Date().getTime(),
                system: true
            })
            cb()
        })
}

const CreateChatRoom = ({ navigation }) => {

    const [roomName, setRoomName] = useState()

    const CreateChat = () => {
        createChatRoom(roomName, () => {
            navigation.navigate("chat_room")
        })
    }

    return (
        <Box flex={1} justifyContent="center" >
            <Stack width='100%' padding={5}>
                <Center><Text fontWeight='bold' fontSize="5xl">Create Room</Text>
                <Input placeholder="Room Name" w='64' my={5} _focus={{ borderColor: 'info.300' }} onChangeText={setRoomName} />
                </Center>
                <Button title="Create Room" onPress={CreateChat}/>
           </Stack>
        </Box>
    )
}

export default CreateChatRoom
