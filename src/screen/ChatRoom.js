import React, { useState, useEffect } from 'react'
import {
    View,
    StyleSheet,
    Text,
    FlatList,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native'
import firestore from '@react-native-firebase/firestore'

const ChatRoom = ({navigation}) => {
    const [threads, setThreads] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        const unsubscribe = firestore()
            .collection('MESSAGE_THREADS')
            .orderBy('latestMessage.createdAt', 'desc')
            .onSnapshot(querySnapshot => {
                const threads = querySnapshot.docs.map(documentSnapshot => {
                    return {
                        _id: documentSnapshot.id,
                        name: '',
                        latestMessage: { text: '' },
                        ...documentSnapshot.data()
                    }
                })

                setThreads(threads)
                console.log('fasfbhagwgfashvbfaghsvfashbfvuak',threads)
                if (loading) {
                    setLoading(false)
                }
            })
        return () => unsubscribe()
    }, [])

    if (loading) {
        return <ActivityIndicator size='large' color='#555' />
    }


    return (
        <View style={styles.container}>
          <FlatList
            data={threads}
            keyExtractor={item => item._id}
            renderItem={({ item }) => (
              <TouchableOpacity onPress={() => navigation.navigate('chat_screen', { thread: item })}>
                <View style={styles.row}>
                  <View style={styles.content}>
                    <View>
                      <Text style={styles.TextName}>{item.name}</Text>
                    </View>
                    <Text style={styles.contentText}>
                      {item.latestMessage.text}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            )}
            ItemSeparatorComponent={() => <View style={styles.separate} />}
          />
        </View>
      )

}

export default ChatRoom

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'whitesmoke'
    },
    title: {
      marginTop: 20,
      marginBottom: 30,
      fontSize: 28,
      fontWeight: '500'
    },
    row: {
      paddingRight: 50,
      paddingLeft: 10,
      paddingVertical: 5,
      flexDirection: 'row',
      alignItems: 'center'
    },
    content: {
      flexShrink: 1
    },
    header: {
      flexDirection: 'column'
    },
    TextName: {
      fontWeight: '600',
      fontSize: 22,
      color: 'black'
    },
    contentText: {
      color: 'slateblack',
      fontSize: 16,
      marginTop: 2
    },
    separate: {
        backgroundColor: '#555',
        paddingLeft: 20,
        height: 0.4,
      }
  })
