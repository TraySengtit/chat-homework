import { createStackNavigator } from '@react-navigation/stack';
import { Avatar } from 'native-base';
import React, { useContext } from 'react'
import { View, Text, Button } from 'react-native'
import Login from '../components/Login';
import SignIn from '../components/SignIn';
import { AuthContext } from '../provider/AuthProvider';
import ChatRoom from '../screen/ChatRoom';
import ChatScreen from '../screen/ChatScreen';
import CreateRoom from '../screen/CreateChatRoom';

const Stack = createStackNavigator();

const StackNavigation = () => {
    let { auth, setAuth } = useContext(AuthContext)

    const Stack = createStackNavigator();
    return (
        <Stack.Navigator>

            <Stack.Screen name="login" component={Login}
                options={() => ({
                    title: 'Log Out',
                    headerShown: false
                })}
            />

            <Stack.Screen name="singup" component={SignIn}
                options={() => ({
                    title: 'sign up',
                    headerShown: false
                })}
            />

            <Stack.Screen name="chat_room" component={ChatRoom}
                options={({ navigation }) => ({
                    title: 'Chat Room',
                    headerRight: () => (
                        <>
                            {/* <Avatar
                                size="sm"
                                source={{
                                    // uri: "https://ui-avatars.com/api/?name=" + auth.payload.username,
                                }}
                                mx={2}
                            /> */}
                            <Button title=" + "
                                onPress={() => navigation.navigate("create_room")}
                            />
                        </>
                    ),
                    headerStatusBarHeight: 50
                })}
            />

            <Stack.Screen name="create_room" component={CreateRoom} options={{ title: 'Create Room' }} />
            <Stack.Screen name="chat_screen" component={ChatScreen}
                options={({ route }) => ({
                    title: route.params.thread.name
                })} />

        </Stack.Navigator>
    )
}

export default StackNavigation
